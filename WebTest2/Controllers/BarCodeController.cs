﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using ZipConvertCustomerBarCode;

namespace WebTest2.Controllers
{
    public class BarCodeController : ApiController
    {
        // POST api/barcode
        public async Task<string> Post()
        {
            try
            {
                var param = HttpUtility.UrlDecode(await Request.Content.ReadAsStringAsync()).Split('&').Select(x => x.Split('=')).ToDictionary(x => x[0], x => x[1]);

                using (var img = BarCode.CreateImage(param["zipCode"], param["address"]))
                using (var ms = new MemoryStream())
                {
                    img.Save(ms, ImageFormat.Gif);

                    // base64エンコーディングを行い画像を文字列として保存する
                    return "data:image/gif;base64," + Convert.ToBase64String(ms.GetBuffer());

                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e.ToString());
                throw e;
            }
        }
    }
}
