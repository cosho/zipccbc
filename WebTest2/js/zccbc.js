﻿$(function () {
    $('#genarate').on('click', generate);
});
function generate() {

    var zipCode = $('#zipCode').val();
    var address = $('#address').val();
	if(zipCode == ''|| address == ''){
		errorAlert('郵便番号、住所は必須入力です。');
	}
    new $.ajax({
        url: "api/BarCode",
        type: "POST",
        data: { zipCode: zipCode, address: address },
        dataType: "text",
        contentType: "application/json",
        success: function (data, dataType) {
            $("#barCode").prop('src', data.replace(/"/g, ""));
            $("#errorMessage").html('');
            $("#errorMessage").removeClass('alert alert-danger');
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            errorAlert(JSON.parse(XMLHttpRequest.responseText).ExceptionMessage);
        }
    });
}
function errorAlert(message){
	$("#errorMessage").html(message);
    $("#barCode").prop('src', '');
    $("#errorMessage").addClass('alert alert-danger');
}